let income = 0;
let balance = 0;
let loanAmount = 0;

/* 
By pushing the loan button you can more money into balance, but also increases your loan.
This will also disable the loan button until the loan is repaid
*/
function getLoan(){
    loanAmount = Number(window.prompt("How much do you want to loan?"));

    if (loanAmount > 2*balance){
        window.alert("You can't loan THAT much money from us");
        exit()
    }
    else if (loanAmount <= 0){
        window.alert("You have to loan more than 0 kr");
        exit()
    }
    else if (isNaN(loanAmount)){
        window.alert("You have to put in a number >:(");
        exit()
    }
    else{ //Else removed a bug where with the loanAmount was added once per attempt. Even failed attempts
        balance += loanAmount;
        document.getElementById("balance").innerHTML = Intl.NumberFormat('no-NO', { minimumFractionDigits: 0, style: 'currency', currency: 'NOK' }).format(balance);
        document.getElementById("outstandingLoan").innerHTML = Intl.NumberFormat('no-NO', { minimumFractionDigits: 0, style: 'currency', currency: 'NOK' }).format(loanAmount);
    
        document.getElementById("loan").disabled = true;
        document.getElementById("repayLoan").hidden = false;
    }
}

/*
Each push of the "work" button gives you 500 kr which can be used to pay loan or put into bank
in other functions. 
*/
function getPaid(){
    income += 500;
    document.getElementById("pay").innerHTML = Intl.NumberFormat('no-NO', { minimumFractionDigits: 0, style: 'currency', currency: 'NOK' }).format(income);
}

/*
Takes 10% of your income to pay loan and the rest into the bank.
If there is no loan, 100% of income will go into the bank
*/
function putMoneyInBank(){
    if (income*0.1 >= loanAmount){
        loanAmount -= income*0.1
        balance += income*0.9 - loanAmount
        loanAmount = 0;
    }
    else{
        loanAmount -= income*0.1;
        balance += income*0.9;    
    }

    if (loanAmount === 0){
        document.getElementById("loan").disabled = false;
        document.getElementById("repayLoan").hidden = true;
    }
    
    income = 0;
    document.getElementById("pay").innerHTML = Intl.NumberFormat('no-NO', { minimumFractionDigits: 0, style: 'currency', currency: 'NOK' }).format(income);
    
    document.getElementById("outstandingLoan").innerHTML = Intl.NumberFormat('no-NO', { minimumFractionDigits: 0, style: 'currency', currency: 'NOK' }).format(loanAmount);
    document.getElementById("balance").innerHTML = Intl.NumberFormat('no-NO', { minimumFractionDigits: 0, style: 'currency', currency: 'NOK' }).format(balance);
}

/*
all the income will go into paying off the loan. Any excess wil remain in income 
*/
function repayLoan(){
    if (income > loanAmount){
        income -= loanAmount;
        loanAmount = 0;
    } 
    else if(income < loanAmount){     
        loanAmount -= income;
        income = 0;
    }
    else{
        income = 0;
        loanAmount = 0;
    }

    if (loanAmount === 0){
        document.getElementById("loan").disabled = false;
        document.getElementById("repayLoan").hidden = true;
    }

    document.getElementById("pay").innerHTML = Intl.NumberFormat('no-NO', { minimumFractionDigits: 0, style: 'currency', currency: 'NOK' }).format(income);
    document.getElementById("outstandingLoan").innerHTML = Intl.NumberFormat('no-NO', { minimumFractionDigits: 0, style: 'currency', currency: 'NOK' }).format(loanAmount);
    document.getElementById("balance").innerHTML = Intl.NumberFormat('no-NO', { minimumFractionDigits: 0, style: 'currency', currency: 'NOK' }).format(balance);
}

/*
Uses balance to pay for a computer. Prices changes depending of which computer is selected
*/
function buyComputer(){
    let price = priceElement.innerText.replace("NOK", '');
    if (price > balance){
        window.alert("You're broke dude")
    }
    else{
        balance -= price;
        price = 0;
    }
    document.getElementById("balance").innerHTML = Intl.NumberFormat('no-NO', { minimumFractionDigits: 0, style: 'currency', currency: 'NOK' }).format(balance);
}

//Creating API
const computersElement = document.getElementById("computers");
const titleElement = document.getElementById("title");
const descriptionElement = document.getElementById("description");
const specsElement = document.getElementById("specs");
const priceElement = document.getElementById("price");
const stockElement = document.getElementById("stock");
const activeElement = document.getElementById("active");
const imageElement = document.getElementById("image");
const featuresElement = document.getElementById("features");

let computers = [];

/*
Fetches all the JSON data and creates each computer with their given data
*/
fetch("https://hickory-quilled-actress.glitch.me/computers")
    .then(response => response.json())
    .then(data => computers = data)
    .then(computers => addComputersToMenu(computers))

    const addComputersToMenu = (computers) => {
        computers.forEach(computer => addComputerToMenu(computer));
        const selectedComputer = computers[0];
        featuresElement.innerText = selectedComputer.specs;
        computerImageChange(selectedComputer)
        computerTitleChange(selectedComputer)
        computerDescriptionChange(selectedComputer)
        computerPriceChange(selectedComputer)
    }

    const addComputerToMenu = (computer) => {
        const computerElement = document.createElement("option");
        computerElement.value = computer.id;
        computerElement.appendChild(document.createTextNode(computer.title));
        computersElement.appendChild(computerElement);
    }

/*
Displays all the data from the chosen computer. Deals with title, price, description, etc.
*/
function handleComputerChange() {
    const selectedComputer = computers[computersElement.selectedIndex];
    featuresElement.innerText = selectedComputer.specs;
    computerImageChange(selectedComputer)
    computerTitleChange(selectedComputer)
    computerDescriptionChange(selectedComputer)
    computerPriceChange(selectedComputer)
}

/*
Handles the changing of computers
*/
computersElement.addEventListener("change",() => handleComputerChange());

/*
Changes the image given the computer
*/
function computerImageChange(computer) {
    imageElement.innerText = "";
    const newImageElement = document.createElement("img")
    newImageElement.src = `https://hickory-quilled-actress.glitch.me/${computer.image}`
    newImageElement.alt = "Image not found"
    newImageElement.setAttribute("class", "img-thumbnail");
    document.getElementById("image").appendChild(newImageElement);
}

/*
Changes the title given the computer
*/
function computerTitleChange(computer) {
    titleElement.innerText = "";
    const newTitleElement = document.createElement("h2");
    newTitleElement.innerText = computer.title;
    document.getElementById("title").appendChild(newTitleElement);
}

/*
Changes the description given the computer
*/
function computerDescriptionChange(computer) {
    descriptionElement.innerText = "";
    const newDescriptionElement = document.createElement("p");
    newDescriptionElement.innerText = computer.description;
    document.getElementById("description").appendChild(newDescriptionElement)
}

/*
Changes the price given the computer
*/
function computerPriceChange(computer) {
    priceElement.innerText = "";
    const newPriceElement = document.createElement("h1");
    newPriceElement.setAttribute("class", "bold")
    newPriceElement.innerText = computer.price + " NOK";
    document.getElementById("price").appendChild(newPriceElement)
}